import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio1 {
    public static void main(String[] args) {
        List<String> lista = new ArrayList<String>(Arrays.asList(args));
        ProcessBuilder pb = new ProcessBuilder(lista);
        try {
            Process p = pb.start();
            /**
             * El método waitFor espera a que un programa finalice
             * y lo que obtenemos es el código de error
             * 
             * Busca las sobrecargas del método en la documentación
             */
            int exitValue = p.waitFor();
            InputStream is= p.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            Scanner sc = new Scanner(br);
            /**
             * Falta crear el file writter.
             */
            File f = new File("../output.txt");
            /**
             * Comprobamos si se ha ejecutado correctamente
             * Si no, entonces hemos de mostrar el error:
             */
            if (exitValue == 0) {
                while (sc.hasNext()) {
                    String linea = sc.nextLine();
                    System.out.println(linea);
                    //Serializar línea al archivo
                }
            } else {
                //Obtener error.... p.getErrorStream();
            }
        } catch (IOException e) {
            System.out.println("No se puede ejecutar el proceso.");
        } catch (IndexOutOfBoundsException x){
            System.out.println("No se puede ejecutar porque no hay ningun argumento.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
