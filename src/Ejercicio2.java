import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio2 {
    public static void main(String[] args) {
        /**
         * Para poder hacerlo, tenias que redirigir la entrada al teclado 
         * y la salida del proceso por streams:
         */
        String command = "java -jar /home/batoi/IdeaProjects/Ramdom10/out/artifacts/Ramdom10_jar/Ramdom10.jar";

        List<String> lista = new ArrayList<String>(Arrays.asList(command.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(lista);
        pb.redirectInput(ProcessBuilder.Redirect.INHERIT);
        //pb.redirectOutput(new File("./palabras.txt"));
        try {
            /**
             * Ahora lo que tendrías es lo mismo que en el primer proceso
             */
            Process process = pb.start();
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
