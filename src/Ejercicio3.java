import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Ejercicio3 {
    public static void main(String[] args) {
        String command = "java -jar /home/batoi/IdeaProjects/Minusculas/out/artifacts/Minusculas_jar/Minusculas.jar";

        List<String> lista = new ArrayList<String>(Arrays.asList(command.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(lista);

        try {
            Process process = pb.start();

            process.waitFor(100, TimeUnit.MILLISECONDS);

            OutputStream os = process.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            Scanner procesoSC = new Scanner (process.getInputStream());

            Scanner sc = new Scanner(System.in);

            String linea = sc.nextLine();

            while (!linea.equals("stop")) {
                bw.write(linea);
                bw.newLine();
                bw.flush();
                process.waitFor(100, TimeUnit.MILLISECONDS);
                System.out.println(procesoSC.nextLine());
                linea = sc.nextLine();
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
